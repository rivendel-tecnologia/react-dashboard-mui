import { NAMESPACE } from 'api/actions';
import { combineReducers } from 'redux';
import router from 'api/reducers/router';
import dashboard from 'api/reducers/dashboard';
import user from 'api/reducers/user';

export default {
  [NAMESPACE]: combineReducers({
    router,
    dashboard,
    user,
  }),
};
