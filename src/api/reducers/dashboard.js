import actions from 'api/actions';

export default (state = {
  sideMenu: {
    open: true,
  },
}, action) => {
  if (action.type === actions.TOGGLE_SIDE_MENU) {
    return {
      sideMenu: {
        open: !state.sideMenu.open,
      },
    };
  }
  return state;
};
