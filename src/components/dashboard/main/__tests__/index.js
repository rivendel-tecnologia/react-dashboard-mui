import React from 'react';
import { createShallow } from 'material-ui/test-utils';

import Main from '../index';

describe('<Main />', () => {
  const shallow = createShallow({ dive: true });
  const main = shallow(<Main>Hello World!</Main>);

  it('renders properly', () => {
    expect(main).toMatchSnapshot();
  });
});
