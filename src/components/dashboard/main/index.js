import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import classnames from 'classnames';

const styles = theme => ({
  content: {
    backgroundColor: theme.palette.background.default,
    width: '100%',
    overflowX: 'hidden',
    height: `calc(100% - ${theme.spacing.unit * 7}px)`,
    transform: `translate(0px, ${theme.spacing.unit * 7}px)`,
    [theme.breakpoints.up('sm')]: {
      height: `calc(100% - ${theme.spacing.unit * 8}px)`,
      transform: `translate(0px, ${theme.spacing.unit * 8}px)`,
    },
  },
});

const Main = ({
  children, classes, className, style,
}) => (
  <main className={classnames(classes.content, className)} style={style}>
    {children}
  </main>
);

Main.defaultProps = {
  className: null,
};

Main.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  classes: PropTypes.shape({
    root: PropTypes.string,
    appBar: PropTypes.string,
  }).isRequired,
  className: PropTypes.string,
};

export default withStyles(styles)(Main);
