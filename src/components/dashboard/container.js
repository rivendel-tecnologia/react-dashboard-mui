import { connect } from 'react-redux';
import Dashboard from './index';
import { NAMESPACE } from 'api/actions';

const mapStateToProps = state => ({
  open: state.get(NAMESPACE).dashboard.sideMenu.open,
});

export default connect(mapStateToProps)(Dashboard);
