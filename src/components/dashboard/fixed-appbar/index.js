import React from 'react';
import PropTypes from 'prop-types';
import AppBar from 'material-ui/AppBar';
import { withStyles } from 'material-ui/styles';
import classNames from 'classnames';

const styles = () => ({
  appBar: {
    width: 'inherit',
    position: 'fixed',
    top: '0',
    left: 'auto',
    right: 'auto',
  },
});

const FixedAppbar = ({
  children,
  classes,
  className,
  style,
}) => (
  <AppBar
    className={classNames(classes.appBar, className)}
    position="fixed"
    style={style}
  >
    {children}
  </AppBar>
);

FixedAppbar.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  classes: PropTypes.shape({
    appBar: PropTypes.string,
  }).isRequired,
};

export default withStyles(styles)(FixedAppbar);
