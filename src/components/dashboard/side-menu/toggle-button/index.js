import React from 'react';
import PropTypes from 'prop-types';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';

const SideMenuToggle = ({ toggleSideMenu }) => (
  <IconButton
    color="default"
    aria-label="Menu"
    onClick={toggleSideMenu}
  >
    <MenuIcon />
  </IconButton>
);

SideMenuToggle.propTypes = {
  toggleSideMenu: PropTypes.func.isRequired,
};

export default SideMenuToggle;
