import React from 'react';
import PropTypes from 'prop-types';
import Drawer from 'material-ui/Drawer';
import Hidden from 'material-ui/Hidden';
import { withStyles } from 'material-ui/styles';
import classNames from 'classnames';

const drawerWidth = 240;

const styles = theme => ({
  sideMenu: {
    position: 'fixed',
    height: '100%',
    marginLeft: '-1px',
    overflow: 'hidden',
  },
  sideMenuOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  sideMenuClose: {
    width: '0px',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
});

class SideMenu extends React.PureComponent {
  render() {
    const { children, classes, open } = this.props;
    return (
      <React.Fragment>
        <Hidden smDown>
          <Drawer
            classes={{
              paper: classNames(classes.sideMenu, open ? classes.sideMenuOpen : classes.sideMenuClose),
            }}
            variant="permanent"
            open={open}
          >
            {children}
          </Drawer>
        </Hidden>
        <Hidden mdUp>
          <Drawer
            variant="temporary"
            open={open}
            classes={{
              paper: classNames(classes.sideMenu, open ? classes.sideMenuOpen : classes.sideMenuClose),
            }}
            onClose={this.props.toggleSideMenu}
            ModalProps={{
              keepMounted: true,
            }}
          >
            {children}
          </Drawer>
        </Hidden>
      </React.Fragment>
    );
  }
}

SideMenu.defaultProps = {
  children: null,
  open: false,
};

SideMenu.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  classes: PropTypes.shape({
    sideMenu: PropTypes.string,
    sideMenuOpen: PropTypes.string,
    sideMenuClose: PropTypes.string,
  }).isRequired,
  open: PropTypes.bool,
  toggleSideMenu: PropTypes.func.isRequired,
};

export default withStyles(styles)(SideMenu);
