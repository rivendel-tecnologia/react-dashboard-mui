import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Divider from 'material-ui/Divider';
import { withStyles } from 'material-ui/styles';
import { NAMESPACE } from 'api/actions';
import classNames from 'classnames';

const styles = theme => ({
  sideMenuHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    height: theme.mixins.toolbar.minHeight,
  },
  sideMenuOpen: {},
  sideMenuClose: {},
});

const SideMenuHeader = ({
  classes,
  children,
  open,
  className,
  style,
}) => (
  <React.Fragment>
    <div
      className={
        classNames(
          classes.sideMenuHeader, open ? classes.sideMenuOpen : classes.sideMenuClose, className
        )
      }
      style={style}
    >
      {children}
    </div>
    <Divider />
  </React.Fragment>
);

SideMenuHeader.defaultProps = {
  open: true,
};

SideMenuHeader.propTypes = {
  classes: PropTypes.shape({
    sideMenuHeader: PropTypes.string,
    sideMenuOpen: PropTypes.string,
    sideMenuClose: PropTypes.string,
  }).isRequired,
  open: PropTypes.bool,
};

const mapStateToProps = state => ({
  open: state.get(NAMESPACE).dashboard.sideMenu.open,
});

export default connect(mapStateToProps)(withStyles(styles)(SideMenuHeader));
