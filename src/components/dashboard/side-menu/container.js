import { connect } from 'react-redux';
import SideMenu from './index';
import actions, { NAMESPACE } from 'api/actions';

const mapStateToProps = state => ({
  open: state.get(NAMESPACE).dashboard.sideMenu.open,
});

const mapDispatchToProps = dispatch => ({
  toggleSideMenu: () => dispatch({
    type: actions.TOGGLE_SIDE_MENU,
  }),
});

export default connect(mapStateToProps, mapDispatchToProps)(SideMenu);
