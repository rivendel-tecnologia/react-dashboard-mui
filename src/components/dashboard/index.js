import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import classNames from 'classnames';

const drawerWidth = 240;

const styles = theme => ({
  sideMenuOpen: {
    marginLeft: `${drawerWidth}px`,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['margin-left', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  sideMenuClose: {
    width: 0,
    transition: theme.transitions.create(['margin-left', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  root: {
    position: 'absolute',
    height: '100%',
  },
});

class Dashboard extends React.PureComponent {
  render() {
    const {
      classes,
      children,
      open,
      className: classNameProp,
      style,
    } = this.props;

    const className = classNames(
      classes.root,
      open ? classes.sideMenuOpen : classes.sideMenuClose,
      classNameProp,
    );

    return (
      <div className={className} style={style}>
        {children}
      </div>
    );
  }
}

Dashboard.defaultProps = {
  className: '',
  children: null,
  open: false,
};

Dashboard.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  classes: PropTypes.shape({
    root: PropTypes.string,
    appBar: PropTypes.string,
  }).isRequired,
  open: PropTypes.bool,
};

export default withStyles(styles)(Dashboard);
