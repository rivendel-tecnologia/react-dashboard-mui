import React from 'react';
import PropTypes from 'prop-types';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';

const Theme = ({ children, customTheme }) => {
  const theme = createMuiTheme({
    ...customTheme,
  });

  return (
    <MuiThemeProvider theme={theme}>
      {children}
    </MuiThemeProvider>
  );
};

Theme.defaultProps = {
  customTheme: null,
};

Theme.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  customTheme: PropTypes.shape({ '*': PropTypes.object }),
};

export default Theme;
