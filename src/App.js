import React, { Component } from 'react';

import { Provider, connect } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { ConnectedRouter, routerMiddleware, push } from 'react-router-redux';
import { combineReducers } from 'redux-immutable';
import createHistory from 'history/createBrowserHistory';

import { Route, Link } from 'react-router-dom';
import { Switch } from 'react-router';

import List, { ListItem, ListItemText, ListItemIcon } from 'material-ui/List';
import { withStyles, createMuiTheme } from 'material-ui/styles';
import blue from 'material-ui/colors/blue';
import blueGrey from 'material-ui/colors/blueGrey';

import AppsIcon from 'material-ui-icons/Apps';
import DashboardIcon from 'material-ui-icons/Dashboard';
import AssignmentIcon from 'material-ui-icons/Assignment';
import SettingsIcon from 'material-ui-icons/Settings';

import { reducers, sagas, firebase, actions } from './api';

import {
  Theme,
  AppSearch,
  DashboardContainer as Dashboard,
  FixedAppbar,
  Toolbar,
  SideMenuContainer as SideMenu,
  SideMenuHeader,
  SideMenuToggleContainer as SideMenuToggle,
  Main,
  LoginFormContainer as LoginForm,
  RegisterFormContainer as RegisterForm,
  RecoverPasswordFormContainer as RecoverPasswordForm,
  AuthMenu,
} from './components';

import logo from './logo.png';

const muiTheme = createMuiTheme({
  palette: {
    primary: { light: blue[300], main: blue[500], dark: blue[700] },
    secondary: { light: blueGrey[300], main: blueGrey[500], dark: blueGrey[700] },
  },
});

const styles = theme => ({
  dashboardSideMenuOpen: {
    [theme.breakpoints.down('sm')]: {
      width: 'calc(100%)',
      marginLeft: '0',
    },
  },
  dashboardSideMenuClose: {
    [theme.breakpoints.up('lg')]: {
      width: 'calc(100% - 60px)',
      marginLeft: '60px',
    },
    [theme.breakpoints.down('md')]: {
      width: 'calc(100%)',
    },
  },
  miniSideMenu: {
    [theme.breakpoints.up('lg')]: {
      width: '60px',
    },
  },
  headerSideMenu: {
    backgroundImage: `url(${logo})`,
    backgroundSize: '170px',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
  headerSideMenuOpen: {
    backgroundPosition: 'center',
    transition: theme.transitions.create('background-position', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.short * 2,
    }),
  },
  headerSideMenuClose: {
    backgroundPosition: '2px center',
    transition: theme.transitions.create('background-position', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.shortest,
    }),
  },
});

const CustomDashboard = connect((state) => {
  const user = state.get('REACT_DASHBOARD_MUI').user.state;
  return {
    user: (user) ? user : {}
  }
})(withStyles(styles)(props => (
  <Dashboard
    classes={{
      sideMenuOpen: props.classes.dashboardSideMenuOpen,
      sideMenuClose: props.classes.dashboardSideMenuClose,
    }}
  >
    <FixedAppbar>
      <Toolbar>
        <SideMenuToggle />
        <AppSearch />
        <AuthMenu
          style={{
            position: 'absolute',
            right: '16px',
          }}
          userInfo={{
            name: props.user.displayName,
            email: props.user.email,
            photo: props.user.photoURL,
          }}
        />
      </Toolbar>
    </FixedAppbar>
    <SideMenu
      classes={{
        sideMenuClose: props.classes.miniSideMenu,
      }}
    >
      <SideMenuHeader
        classes={{
          sideMenuHeader: props.classes.headerSideMenu,
          sideMenuOpen: props.classes.headerSideMenuOpen,
          sideMenuClose: props.classes.headerSideMenuClose,
        }}
      />
      <List>
        {props.links}
      </List>
    </SideMenu>
    <Main>
      <div style={{ padding: 16 }}>
        {props.pages}
      </div>
    </Main>
  </Dashboard>
)));

class App extends Component {
  componentWillMount() {
    this.history = createHistory();
    const sagaMiddleware = createSagaMiddleware();
    const middlewares = [
      routerMiddleware(this.history),
      sagaMiddleware,
    ];
    const combinedReducers = combineReducers(reducers);
    const composeEnhancers = composeWithDevTools({});
    const composed = composeEnhancers(applyMiddleware(...middlewares));
    this.store = createStore(combinedReducers, composed);
    sagaMiddleware.run(sagas, firebase);
  }

  createMenuItem = (pageHref, pageName, icon) => (
    <ListItem key={pageHref} button component={Link} to={pageHref}>
      {icon && (
        <ListItemIcon>
          {icon}
        </ListItemIcon>
      )}
      <ListItemText primary={pageName} />
    </ListItem>
  );

  createPageRoute = (pageHref, component) => (
    <Route key={pageHref} path={pageHref} component={component} />
  );

  render() {
    const links = [
      this.createMenuItem('/recursos', 'Recursos', <AppsIcon />),
      this.createMenuItem('/monitoramento', 'Monitoramento', <DashboardIcon />),
      this.createMenuItem('/logs', 'Logs', <AssignmentIcon />),
      this.createMenuItem('/configuracoes', 'Configurações', <SettingsIcon />),
    ];

    const pages = [
      this.createPageRoute('/aplicacoes', () => <div>Aplicacações</div>),
      this.createPageRoute('/monitoramento', () => <div>Monitoramento</div>),
      this.createPageRoute('/logs', () => <div>Logs</div>),
      this.createPageRoute('/configuracoes', () => <div>Configurações</div>),
    ];

    return (
      <Provider store={this.store}>
        <ConnectedRouter history={this.history}>
          <Theme customTheme={muiTheme}>
            <Switch>
              <Route path="/login" component={LoginForm} />
              <Route path="/register" component={RegisterForm} />
              <Route path="/recover-password" component={RecoverPasswordForm} />
              <Route
                path="/"
                render={() => {
                  firebase.auth().onAuthStateChanged((user) => {
                    if (!user) {
                      this.store.dispatch(push('/login'));
                    } else {
                      this.store.dispatch({
                        type: actions.LOGIN_USER_SUCCESSFUL,
                        payload: user,
                      });
                    }
                  });
                  return (
                    <CustomDashboard links={links} pages={pages} />
                  );
                }}
              />
            </Switch>
          </Theme>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;
