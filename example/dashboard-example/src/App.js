import React, { Component } from 'react';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { ConnectedRouter, routerMiddleware, push } from 'react-router-redux';
import { combineReducers } from 'redux-immutable';
import createHistory from 'history/createBrowserHistory';
import { reducers, sagas, firebase } from 'react-dashboard-mui/Api';

import { Route, Link } from 'react-router-dom';
import { Switch } from 'react-router';

import List, { ListItem, ListItemText } from 'material-ui/List';
import { createMuiTheme } from 'material-ui/styles';
import green from 'material-ui/colors/green';
import blueGrey from 'material-ui/colors/blueGrey';


import {
  Theme,
  AppSearch,
  DashboardContainer as Dashboard,
  FixedAppbar,
  Toolbar,
  SideMenuContainer as SideMenu,
  SideMenuHeader,
  SideMenuToggleContainer as SideMenuToggle,
  Main,
  LoginFormContainer as LoginForm,
  RegisterFormContainer as RegisterForm,
  RecoverPasswordFormContainer as RecoverPasswordForm,
  AuthMenu,
} from 'react-dashboard-mui/Components';

const theme = createMuiTheme({
  palette: {
    primary: { light: green[300], main: green[500], dark: green[700] },
    secondary: { light: blueGrey[300], main: blueGrey[500], dark: blueGrey[700] },
  },
});

class App extends Component {
  componentWillMount() {
    this.history = createHistory();
    const sagaMiddleware = createSagaMiddleware();
    const middlewares = [
      routerMiddleware(this.history),
      sagaMiddleware,
    ];
    const combinedReducers = combineReducers(reducers);
    const composeEnhancers = composeWithDevTools({});
    const composed = composeEnhancers(applyMiddleware(...middlewares));
    this.store = createStore(combinedReducers, composed);
    sagaMiddleware.run(sagas, firebase);
  }

  createMenuItem = (pageHref, pageName) => (
    <ListItem key={pageHref} button component={Link} to={pageHref}>
      <ListItemText primary={pageName} />
    </ListItem>
  );

  createPageRoute = (pageHref, component) => (
    <Route key={pageHref} path={pageHref} component={component} />
  );

  render() {
    const links = [
      this.createMenuItem('/aplicacoes', 'Aplicacações'),
      this.createMenuItem('/monitoramento', 'Monitoramento'),
      this.createMenuItem('/logs', 'Logs'),
      this.createMenuItem('/configuracoes', 'Configurações'),
    ];

    const pages = [
      this.createPageRoute('/aplicacoes', () => <div>Aplicacações</div>),
      this.createPageRoute('/monitoramento', () => <div>Monitoramento</div>),
      this.createPageRoute('/logs', () => <div>Logs</div>),
      this.createPageRoute('/configuracoes', () => <div>Configurações</div>),
    ];

    return (
      <Provider store={this.store}>
        <ConnectedRouter history={this.history}>
          <Theme customTheme={theme}>
            <Switch>
              <Route path="/login" component={LoginForm} />
              <Route path="/register" component={RegisterForm} />
              <Route path="/recover-password" component={RecoverPasswordForm} />
              <Route
                path="/"
                render={() => {
                  firebase.auth().onAuthStateChanged((user) => {
                    if (!user) {
                      this.store.dispatch(push('/login'));
                    }
                  });
                  return (
                    <Dashboard>
                      <FixedAppbar>
                        <Toolbar>
                          <SideMenuToggle />
                          <AppSearch />
                        </Toolbar>
                      </FixedAppbar>
                      <SideMenu>
                        <SideMenuHeader>
                          <AuthMenu userInfo={{
                              name: 'John Doe',
                              email: 'johndoe@gmail.com',
                            }}
                          />
                        </SideMenuHeader>
                        <List>
                          {links}
                        </List>
                      </SideMenu>
                      <Main>
                        <div style={{padding: 16}}>
                          {pages}
                        </div>
                      </Main>
                    </Dashboard>
                  );
                }}
              />
            </Switch>
          </Theme>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;
